@extends('layouts.app')

@section('content')
    @if(count($posts) > 0)
        <img src="https://user-images.githubusercontent.com/1915268/67271462-31600380-f4d8-11e9-9143-18e197b26f48.png" alt="" class="w-100">
        <h1 class="text-center">Featured Posts:</h1>
            @foreach($posts as $post)
                <div class="card text-center">
                    <div class="card-body">
                        <h4 class="card-title mb-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h4>
                        <h6 class="card-text mb-3">Author: {{$post->user->name}}</h6>
                   
                    </div>
                
                </div>
        @endforeach
    @else
        <div>
            <h2>There are no posts to show</h2>
    <a href="/posts/create" class="btn btn-info">Create post</a>
        </div>
    @endif

@endsection