<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',  [PostController::class, 'featured']);

//route to return a view where the user can create a post
Route::get('/posts/create', [PostController::class, 'create']);

//route for a route wherein form data can be sent via post method
Route::post('/posts', [PostController::class, 'store']);

//route that will return a view containing all posts
Route::get('/posts', [PostController::class, 'index']);


Route::get('/welcome', [PostController::class, 'featured']);

//route that will return only the auth user posts
Route::get('/posts/myPosts', [PostController::class, 'myPosts'] );

//route that will show specific posts view based on URL Param ID
Route::get('/posts/{id}', [PostController::class, 'show']);

//route to edit specifi post
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);

//route that updates a post
Route::put('/posts/{id}', [PostController::class, 'update']);

//route that deletes a post
Route::delete('/posts/{id}', [PostController::class, 'destroy']);

//route that archives a post
Route::put('/posts/{id}', [PostController::class, 'archive']);

//route that will enable users to like/unlike posts
Route::put('/posts/{id}/like', [PostController::class, 'like']);

//route for add comment
Route::post('/posts/{id}/comment', [PostController::class, 'comment']);



Auth::routes();

Route::get('/home', [HomeController::class, 'index']);
